import mysql.connector

class BD():
	def __init__(self):
		self._connect()
		return

	def _connect(self):
		self.connection = mysql.connector.connect(host='infoweb', database='E155916Z', user='E155916Z', password='E155916Z')
		return

	def getCursor(self):
		try:
			self.connection.ping()
		except:
			self._connect()
		return self.connection.cursor()

	def getRow(self, query):
		cursor = self.getCursor()
		cursor.execute(query)
		row = cursor.fetchone()
		cursor.close()
		return row

	def getRows(self, query):
		cursor = self.getCursor()
		cursor.execute(query)
		rows = cursor.fetchall()
		cursor.close()
		return rows

	def execute(self, query, args=None):
		cursor = self.getCursor()
		cursor.execute(query, args)
		self.connection.commit()
		cursor.close()
		return

	def tableExist(self, tableName):
		cursor = self.getCursor()
		cursor.execute("SHOW TABLES LIKE '" + tableName + "'")
		rows = cursor.fetchall()
		return len(rows) == 1
