import http.client
from urllib.parse import urlencode
import json
from urllib import request
from bd import *
import sys

API = "3MGfTWb4aGZqdcsh5SbOi0Idrve25xiA"

try:
    bd = Bd()

    rows = bd.getRows("SELECT id, numvoie, nomvoie, lieudit, commune from installations where longitude = 0.0 and latitude = 0.0")
    for row in rows:
        location = row[1]+" "+row[2]+" "+row[3]+" "+row[4]+" "+row[5]+" France"

        print(location)

        urlParams = {'location': location, 'key': API, 'inFormat':'kvp', 'outFormat':'json'}
        url = "http://www.mapquestapi.com/geocoding/v1/address?" + urlencode(urlParams)

        print(url)
        proxy_host = 'proxyetu.iut-nantes.univ-nantes.prive:3128'
        req = request.Request(url)
        req.set_proxy(proxy_host, 'http')

        response = request.urlopen(req)

        data = response.read().decode('utf8')

        jsonData = json.loads(data)
        # FIXME le print n'est pas très secure...
        print(jsonData['results'][0]['locations'][0]['latLng'])

        latitude = str(jsonData['results'][0]['locations'][0]['latLng']['lat'])
        longitude = str(jsonData['results'][0]['locations'][0]['latLng']['lng'])
        bd.execute("UPDATE installation SET lat = "+latitude+" WHERE id="+str(row[0]))
        bd.execute("UPDATE installation SET long = "+longitude+" WHERE id="+str(row[0]))

except Exception as err:
    print("Unexpected error: {0}".format(err))
